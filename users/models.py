from django.db import models
from django.contrib.auth.models import AbstractUser
# from .manager import CustomUserManager


class User(AbstractUser):
    address = models.CharField(max_length=150)
    email = models.EmailField(unique=True)
    pnumber = models.CharField(max_length=20)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    # object = CustomUserManager()

    # is_member = models.BooleanField()

    def __str__(self):
        return self.username
