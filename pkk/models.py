from django.db import models


def image_upload_path(instance, filename):
    return '/'.join(['SliderImage', filename])


# Create your models here.


class Image(models.Model):
    image = models.ImageField(blank=True,
                              null=True,
                              upload_to=image_upload_path)
    description = models.CharField(max_length=800,
                                   default='This is some Content')
    created_by = models.CharField(max_length=250, default='Admin Admin')

    def __str__(self):
        return self.created_by


class ContactRequest(models.Model):
    subject = models.CharField(max_length=25)
    name = models.CharField(max_length=300)
    email = models.CharField(max_length=300)
    message = models.CharField(max_length=2500)

    def __str__(self):
        return self.name
