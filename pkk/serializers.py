from rest_framework import serializers
from rest_framework.authtoken.models import Token
from . import models


class SliderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Image
        fields = "__all__"


class ContactRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ContactRequest
        fields = "__all__"
