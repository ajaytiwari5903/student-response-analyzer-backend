from django.shortcuts import render
from rest_framework import viewsets
from settings import models as SettingModels
from django.db import transaction
from settings import serializers as SettingSerializer
from rest_framework import status
from rest_framework.response import Response
from rest_framework import decorators
from PIL import Image
from io import BytesIO
import shutil
import sys
import os
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile
from . import serializers
from . import models
from django.core.files.base import ContentFile
import base64
import io
from PIL import Image


class SettingDataViewSets(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = SettingSerializer.SettingSerializer
    queryset = SettingModels.Setting.objects.all()

    @transaction.atomic
    def list(self, request, *args, **kwargs):
        data = {
            'name': 'Please change from setting',
            'address': 'Address',
            'phone_number': 'Phone number',
            'mail_email': 'info@stsolutions.com.np',
            'video_link': 'UCz_jZRh5ybx8Wa-Xlrzba5w',
            'google_key': 'AIzaSyBqdgAaUlrWDo1I5IsbW7lTJadvdHQmWrw',
        }
        if SettingModels.Setting.objects.all().exists():
            setting = SettingModels.Setting.objects.all()[0]
            data['name'] = setting.name
            data['address'] = setting.address
            data['phone_number'] = setting.phone_number
            data['mail_email'] = setting.mail_email
            data['video_link'] = setting.video_link
            data['google_key'] = setting.google_key
            return Response(data)
        return Response(data)


def base64_file(data, name=None):
    _format, _img_str = data.split(';base64,')
    _name, ext = _format.split('/')
    if not name:
        name = _name.split(":")[-1]
    return ContentFile(base64.b64decode(_img_str),
                       name='{}.{}'.format(name, ext))


class SliderViewSets(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.SliderSerializer
    queryset = models.Image.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False}
        required_list = [
            'description',
            'created_by',
        ]
        data = request.data
        if not len(data['image']) > 0:
            responseData['required'] = 'image'
            return Response(responseData)
        for item in required_list:
            if not data[item]:
                responseData['required'] = item
                return Response(responseData)

        images = request.data['image']
        for key in images.keys():
            image = models.Image()
            imgData = base64_file(data=images[key], name=f'{image.id}{key}')
            image.image = imgData
            image.description = data['description']
            image.created_by = data['created_by']
            image.save()
            print(key)
        responseData['success'] = True
        return Response(responseData)

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False, 'error': False}
        required_list = [
            'description',
            'created_by',
        ]
        data = request.data
        if not len(data['image']) > 0:
            responseData['required'] = 'image'
            return Response(responseData)
        for item in required_list:
            if not data[item]:
                responseData['required'] = item
                return Response(responseData)
        instance = self.get_object()
        # old_instance = models.Image.objects.get(
        #     id=(int(self.kwargs.get('pk'))))
        print(len(data['image']))
        images = data['image']
        old_image_path = instance.image
        image_path = '/media/' + str(old_image_path)
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        image_file_actual_path = BASE_DIR + image_path
        print(os.path.exists(image_file_actual_path))
        if os.path.exists(image_file_actual_path):
            os.remove(image_file_actual_path)
            instance.delete()
            for key in images.keys():
                image = models.Image()
                imgData = base64_file(data=images[key],
                                      name=f'{image.id}{key}')
                image.image = imgData
                image.description = data['description']
                image.created_by = data['created_by']
                image.save()
                print(key)
            responseData['success'] = True
            return Response(responseData)
        else:
            responseData['error'] = True
            return Response(responseData)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        old_image_path = instance.image
        image_path = '/media/' + str(old_image_path)
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        image_file_actual_path = BASE_DIR + image_path
        print(os.path.exists(image_file_actual_path))
        if os.path.exists(image_file_actual_path):
            os.remove(image_file_actual_path)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def list(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False}
        print('testing')
        data = models.Image.objects.all()
        serializer = serializers.SliderSerializer(data, many=True)
        return Response(serializer.data)


class PublicSliderList(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.SliderSerializer
    queryset = models.Image.objects.all()

    def list(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False}
        print('testing')
        data = models.Image.objects.all()
        serializer = serializers.SliderSerializer(data, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        image_path = serializer.data['image'].split('/media')
        image_path = '/media' + image_path[1]
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        image_file_actual_path = BASE_DIR + image_path
        data = {
            'image_discription': serializer.data['description'],
            'image_created_by': serializer.data['created_by'],
            'image': {}
        }
        if os.path.exists(image_file_actual_path):
            with open(image_file_actual_path, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                encoded_string = str(encoded_string).replace(
                    "b'", 'data:image/jpeg;base64,')[:-1]
                # print(encoded_string)
                data['image'] = encoded_string
        return Response(data)


class PublicContactRequest(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.ContactRequestSerializer
    queryset = models.ContactRequest.objects.all()
