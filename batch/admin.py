from django.contrib import admin
from . import models


@admin.register(models.DashboardData)
class DashboardDataAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Batch)
class BatchAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Question)
class QuestionAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Answer)
class AnswerAdmin(admin.ModelAdmin):
    pass