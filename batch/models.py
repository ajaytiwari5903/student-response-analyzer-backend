from django.db import models
# Create your models here.
from django.contrib.postgres.fields import ArrayField


class Batch(models.Model):
    batch = models.CharField(max_length=500)
    chart = models.BooleanField(default=False)
    chartData = models.TextField(blank=True, null=True)
    data = ArrayField(models.IntegerField(), blank=True, null=True)

    def __str__(self) -> str:
        return self.batch


class Question(models.Model):
    question = models.TextField()
    batch = models.ForeignKey(Batch, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.question


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.TextField()

    def __str__(self) -> str:
        return self.answer


class DashboardData(models.Model):
    question = models.CharField(max_length=500)
    answer_summ = models.TextField()
