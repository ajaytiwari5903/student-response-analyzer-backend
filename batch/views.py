from rest_framework import viewsets
from django.db import transaction
from rest_framework.response import Response
from . import serializers
from . import models
from . import background


class BatchView(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.BatchSerializer
    queryset = models.Batch.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False}
        # print(request.data)
        batch = models.Batch.objects.create(batch=request.data['batch'])
        questions = request.data['qustions']
        for item in questions:
            if item['question'] != '':
                models.Question.objects.create(batch=batch,
                                               question=item['question'])
        background.run_background_task()

        responseData['success'] = True
        return Response(responseData)

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False, 'error': False}
        instance = self.get_object()
        questions = models.Question.objects.filter(batch=instance)
        for item in questions:
            item.delete()
        data = request.data
        instance.batch = data['batch']
        instance.save()
        questions = request.data['qustions']
        for item in questions:
            if item['question'] != '':
                models.Question.objects.create(batch=instance,
                                               question=item['question'])

        background.run_background_task()
        responseData['success'] = True
        return Response(responseData)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        data = {'batch': instance.batch, 'question': []}
        questions = models.Question.objects.filter(batch=instance)
        for item in questions:
            data['question'].append({
                'question': item.question,
                'questionId': item.id
            })
        return Response(data)


class QuestionView(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.QuestionSerializer
    queryset = models.Question.objects.all()

    def create(self, request, *args, **kwargs):
        background.run_background_task()
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        background.run_background_task()
        return super().update(request, *args, **kwargs)


class AnswerView(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.AnswerSerializer
    queryset = models.Answer.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        responseData = {'required': '', 'success': False}
        data = request.data['data']
        for item in data:
            models.Answer.objects.create(
                answer=item['answer'],
                question=models.Question.objects.get(id=item['questionId']))

        background.run_background_task()

        responseData['success'] = True
        return Response(responseData)

    def update(self, request, *args, **kwargs):
        background.run_background_task()
        return super().update(request, *args, **kwargs)


class DashboardView(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    serializer_class = serializers.DashboardDataSerializer
    queryset = models.DashboardData.objects.all()