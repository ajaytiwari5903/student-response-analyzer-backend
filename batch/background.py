from apscheduler.schedulers.background import BackgroundScheduler
from summarizer import Summarizer
from apscheduler.executors.pool import ProcessPoolExecutor

from . import models

executors = {
    'default': {
        'type': 'threadpool',
        'max_workers': 1
    },
    'processpool': ProcessPoolExecutor(max_workers=1)
}

scheduler = BackgroundScheduler({'apscheduler.job_defaults.max_instances': 1})
scheduler.configure(executors=executors)
model = Summarizer('distilbert-base-uncased')


def run_model(input_text):
    berttext = ''
    berttext = model(input_text, num_sentences=5)
    return berttext


def process_dashboard_data():
    print("Running data pipeline")

    dashboard_results = []

    questions = models.Question.objects.filter()
    if len(questions) != 0:
        for question in questions:
            answers = models.Answer.objects.filter(question=question)
            if len(answers) != 0:
                answer_text = ''
                for answer in answers:
                    if answer.answer != '':
                        answer_text = answer_text + answer.answer

                print("Executing model.")
                summary_text = run_model(answer_text)

                dashboard_results.append(
                    models.DashboardData(question=question.question,
                                         answer_summ=summary_text))
    print("Clearing database")
    models.DashboardData.objects.all().delete()

    print(f"Saving {len(dashboard_results)} objects into DashboardData.")
    models.DashboardData.objects.bulk_create(dashboard_results)


def run_background_task():
    if not scheduler.running:
        scheduler.start()

    scheduler.configure

    scheduler.add_job(process_dashboard_data)
