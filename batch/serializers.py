from rest_framework import serializers
from rest_framework.authtoken.models import Token
from . import models


class BatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Batch
        fields = "__all__"


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Question
        fields = "__all__"


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answer
        fields = "__all__"


class DashboardDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DashboardData
        fields = "__all__"