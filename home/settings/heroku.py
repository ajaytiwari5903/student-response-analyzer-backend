'''Use this for Heroku deployments.'''

from .base import *

DEBUG = True

ALLOWED_HOSTS += ['isat-full.herokuapp.com', 'localhost', '127.0.0.1']

WSGI_APPLICATION = 'home.wsgi.heroku.application'

CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.filebased.FileBasedCache",
        "LOCATION": "~/.cache/huggingface"
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'db5lphit349g85',
        'USER': 'nazbhpqmkpzvte',
        'PASSWORD':
        'a857b01a92a6f1e6d216b01ce001b03aad7b7177c7aa0f3382e389f4298ddb34',
        'HOST': 'ec2-52-44-209-165.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
        # 'rest_framework.authentication.BasicAuthentication',
    ),
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator'
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator'
    },
]

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

CORS_ORIGIN_WHITELIST = ('http://localhost:8000', 'http://127.0.0.1:8000')
