from rest_framework.routers import DefaultRouter
from users.views import UserViewSets
from batch.views import BatchView, QuestionView, AnswerView, DashboardView
from settings.views import (SettingViewSets)

router = DefaultRouter()

router.register(r'settings', SettingViewSets)
router.register(r'batch', BatchView)
router.register(r'question', QuestionView)
router.register(r'answer', AnswerView)
router.register(r'dashboardData', DashboardView)

urlpatterns = router.urls
