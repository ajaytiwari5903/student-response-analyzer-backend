from django.db import models
from users.models import User


# Create your models here.
def principal_signature_upload_path(instance, filename):
    return '/'.join(['signature', filename])


def logo_upload_path(instance, filename):
    return '/'.join(['logo', filename])


class Setting(models.Model):
    name = models.CharField(max_length=250)
    headmaster_name = models.CharField(max_length=250, blank=True, null=True)
    address = models.CharField(max_length=250)
    video_link = models.CharField(
        max_length=250, default='https://www.youtube.com/watch?v=eKqWrEL-wgk')
    google_key = models.CharField(
        max_length=250, default='AIzaSyBqdgAaUlrWDo1I5IsbW7lTJadvdHQmWrw')
    phone_number = models.CharField(max_length=25)
    principal_signature = models.ImageField(
        blank=True, null=True, upload_to=principal_signature_upload_path)
    logo = models.ImageField(blank=True, null=True, upload_to=logo_upload_path)
    mail_email = models.CharField(max_length=250,
                                  default="info@stsolutions.com.np")
    mail_password = models.CharField(max_length=250, default="admin")
    mail_text = models.CharField(max_length=1000)
    mail_subject = models.CharField(max_length=300)
    sent_mail_to_participant = models.BooleanField(default=True)
