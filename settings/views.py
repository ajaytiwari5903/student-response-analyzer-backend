from django.shortcuts import render
from rest_framework import viewsets
from . import models
from django.db import transaction
from . import serializers
from rest_framework import status
from rest_framework.response import Response
from rest_framework import decorators
from PIL import Image
from io import BytesIO
import shutil
import sys
import os
from django.conf import settings
from django.core.files.uploadedfile import InMemoryUploadedFile


class SettingViewSets(viewsets.ModelViewSet):
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]
    serializer_class = serializers.SettingSerializer
    queryset = models.Setting.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        print(request.data)
        data = request.data
        uploadedImagesig = data['principal_signature']
        uploadedImagelogo = data['logo']
        if models.Setting.objects.all().exists():
            setting = models.Setting.objects.all()[0]
            if (uploadedImagesig) and not isinstance(uploadedImagesig, str):
                imageTemproary = Image.open(uploadedImagesig)
                # imageTemproary = imageTemproary.convert('RGB')
                outputIoStream = BytesIO()
                # imageTemproaryResized = imageTemproary.resize((600, 600))
                imageTemproary.save(outputIoStream, format='PNG', quality=100)
                outputIoStream.seek(0)
                uploadedImagesig = InMemoryUploadedFile(
                    outputIoStream, 'ImageField',
                    "%s.jpg" % uploadedImagesig.name.split('.')[0],
                    'image/png', sys.getsizeof(outputIoStream), None)
                folder_path = os.path.join(settings.MEDIA_ROOT, 'signature')
                old_folder_path = os.path.join(
                    settings.MEDIA_ROOT, str(setting.principal_signature))
                if os.path.exists(old_folder_path):
                    print(setting.principal_signature)
                    print(True)
                    os.remove(old_folder_path)
                setting.principal_signature = uploadedImagesig
            if (uploadedImagelogo) and not isinstance(uploadedImagelogo, str):
                imageTemproary = Image.open(uploadedImagelogo)
                # imageTemproary = imageTemproary.convert('RGB')
                outputIoStream = BytesIO()
                # imageTemproaryResized = imageTemproary.resize((600, 600))
                imageTemproary.save(outputIoStream, format='PNG', quality=100)
                outputIoStream.seek(0)
                uploadedImagelogo = InMemoryUploadedFile(
                    outputIoStream, 'ImageField',
                    "%s.jpg" % uploadedImagelogo.name.split('.')[0],
                    'image/png', sys.getsizeof(outputIoStream), None)
                folder_path = os.path.join(settings.MEDIA_ROOT, 'signature')
                old_folder_path = os.path.join(settings.MEDIA_ROOT,
                                               str(setting.logo))
                if os.path.exists(old_folder_path):
                    print(setting.logo)
                    print(True)
                    os.remove(old_folder_path)
                setting.logo = uploadedImagelogo
            setting.name = data['name']
            setting.address = data['address']
            # setting.principal_signature = data['principal_signature']
            # setting.logo = data['logo']
            setting.mail_text = data['mail_text']
            setting.headmaster_name = data['headmaster_name']
            setting.mail_subject = data['mail_subject']
            setting.phone_number = data['phone_number']
            setting.mail_email = data['mail_email']
            setting.mail_password = data['mail_password']
            setting.video_link = data['video_link']
            setting.google_key = data['google_key']

            setting.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            setting = models.Setting()
            setting.name = data['name']
            setting.principal_signature = data['principal_signature']
            setting.logo = data['logo']
            setting.mail_text = data['mail_text']
            setting.mail_subject = data['mail_subject']
            setting.address = data['address']
            setting.headmaster_name = data['headmaster_name']
            setting.phone_number = data['phone_number']
            setting.mail_email = data['mail_email']
            setting.mail_password = data['mail_password']
            setting.video_link = data['video_link']
            setting.google_key = data['google_key']

            setting.save()
            return Response(status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        data = {
            'name': 'Please change from setting',
            'address': 'Address',
            'headmaster_name': 'Alish Dahal',
            'mail_text': 'Mail Text',
            'mail_subject': 'Mail Subject',
            'phone_number': 'Phone number',
            'mail_email': 'info@stsolutions.com.np',
            'mail_passwrod': 'admin'
        }
        if models.Setting.objects.all().exists():
            setting = models.Setting.objects.all()[0]
            serializer = self.get_serializer(setting)
            print(setting.principal_signature)
            return Response(serializer.data)
        return Response(data)
