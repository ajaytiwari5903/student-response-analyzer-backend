# Generated by Django 2.1.1 on 2020-10-18 05:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('settings', '0009_auto_20201016_0651'),
    ]

    operations = [
        migrations.AddField(
            model_name='setting',
            name='headmaster_name',
            field=models.CharField(blank=True, max_length=250, null=True),
        ),
    ]
